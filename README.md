[![pipeline status](https://gitlab.com/CaiSharp/tracksuit/badges/master/pipeline.svg)](https://gitlab.com/CaiSharp/tracksuit/-/commits/master)
[![coverage report](https://gitlab.com/CaiSharp/tracksuit/badges/master/coverage.svg)](https://gitlab.com/CaiSharp/tracksuit/-/commits/master)

# Tracksuit

## How to run

### Mobile & Server

1. install with `yarn`
2. run `yarn dev`
3. _done_

soon...

## Why did my commit fail?!

- There's a `pre-commit` hook
- `pre-commit` runs `yarn lint` on the project and fails if ESLint detects an error
