FROM node:13-alpine
RUN npm i -g yarn

WORKDIR /app

COPY . .

RUN yarn install --production

WORKDIR /app/packages/server/

EXPOSE 4000

CMD [ "npm", "start" ]