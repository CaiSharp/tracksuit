import React, {useState, useEffect} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {AuthStack} from './src/routes/stackNavigation';
import {
	Home,
	SignUporLogin,
	Loading,
	Login,
	SignUp,
	Presets,
	Track,
	Create,
} from './src/screens';

import {Tab} from './src/routes/tabNavigastion';
import {AuthContext, initUser, logout, login} from './src/context/authContext';
import {User} from './src/services/sharedInterfaces';
import {SafeAreaView} from 'react-native';
import {Icon, StyleService, useStyleSheet, Text} from '@ui-kitten/components';
import {WorkoutStack} from './src/routes/stackNavigation';

export const App: React.FC = () => {
	const [isLoading, setIsLoading] = useState(true);
	const [authState, setAuthState]: [
		User | null | undefined,
		React.Dispatch<React.SetStateAction<User | null | undefined>>,
	] = useState();

	useEffect(() => {
		(async () => {
			const user = await initUser();
			if (user) {
				setAuthState(user);
				setIsLoading(false);
			} else {
				setIsLoading(false);
			}
		})();
	}, []);

	const styles = useStyleSheet(themedStyles);

	if (isLoading) return <Loading />;

	return (
		<SafeAreaView style={styles.safeArea}>
			<AuthContext.Provider
				value={{
					user: authState,
					login: (token) => login(setAuthState, token),
					logout: () => logout(setAuthState),
				}}>
				<NavigationContainer>
					{!authState ? (
						<AuthStack.Navigator
							screenOptions={{
								headerShown: false,
							}}>
							<AuthStack.Screen
								name="SignUpOrLogin"
								component={SignUporLogin}
							/>
							<AuthStack.Screen name="Login" component={Login} />
							<AuthStack.Screen name="SignUp" component={SignUp} />
						</AuthStack.Navigator>
					) : (
						<Tab.Navigator
							screenOptions={({route}) => ({
								tabBarIcon: ({focused, color, size}) => {
									let iconName;

									switch (route.name) {
										case 'Home':
											iconName = focused ? 'bar-chart' : 'bar-chart-outline';
											break;

										case 'Workouts':
											iconName = focused ? 'activity' : 'activity-outline';
											break;
									}

									return (
										<Icon
											style={{width: size, height: size}}
											fill={color}
											name={iconName}
										/>
									);
								},
								tabBarLabel: ({color, focused}) => {
									let labelName;

									switch (route.name) {
										case 'Home':
											labelName = 'Home';
											break;

										case 'Workouts':
											labelName = 'Workouts';
											break;
									}
									return (
										<Text
											style={{
												fontSize: 12,
												color: color,
											}}>
											{labelName}
										</Text>
									);
								},
							})}
							tabBarOptions={{
								activeTintColor: styles.active.backgroundColor,
								inactiveTintColor: styles.inActive.backgroundColor,
								tabStyle: {backgroundColor: styles.safeArea.backgroundColor},
							}}>
							<Tab.Screen name="Home" component={Home} />
							<Tab.Screen
								name="Workouts"
								children={() => (
									<WorkoutStack.Navigator headerMode="none">
										<WorkoutStack.Screen name="Presets" component={Presets} />
										<WorkoutStack.Screen name="Create" component={Create} />
										<WorkoutStack.Screen name="Track" component={Track} />
									</WorkoutStack.Navigator>
								)}
							/>
						</Tab.Navigator>
					)}
				</NavigationContainer>
			</AuthContext.Provider>
		</SafeAreaView>
	);
};

const themedStyles = StyleService.create({
	safeArea: {
		flex: 1,
		backgroundColor: 'background-basic-color-1',
	},
	active: {
		backgroundColor: 'text-primary-hover-color',
	},
	inActive: {
		backgroundColor: 'text-hint-color',
	},
});
