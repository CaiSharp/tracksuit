/*eslint-disable */
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: any }> = { [K in keyof T]: T[K] };

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};



export enum Role {
  Admin = 'ADMIN',
  User = 'USER'
}

export enum WorkoutType {
  Cardio = 'CARDIO',
  FullBody = 'FULL_BODY',
  UpperBody = 'UPPER_BODY',
  LowerBody = 'LOWER_BODY'
}

export type Token = {
  __typename?: 'Token';
  token: Scalars['String'];
};

export type Workout = {
  __typename?: 'Workout';
  id: Scalars['ID'];
  presetId: Scalars['ID'];
  userId: Scalars['ID'];
  name: Scalars['String'];
  createdAt: Scalars['String'];
  duration?: Maybe<Scalars['Float']>;
  calories?: Maybe<Scalars['Int']>;
  distance?: Maybe<Scalars['Float']>;
};

export type Preset = {
  __typename?: 'Preset';
  id: Scalars['ID'];
  userId: Scalars['ID'];
  name: Scalars['String'];
  createdAt: Scalars['String'];
  duration?: Maybe<Scalars['Boolean']>;
  calories?: Maybe<Scalars['Boolean']>;
  distance?: Maybe<Scalars['Boolean']>;
  type: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  username: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
  createdAt: Scalars['String'];
  roles: Array<Role>;
  workouts?: Maybe<Array<Workout>>;
};

export type Query = {
  __typename?: 'Query';
  getWorkoutById?: Maybe<Workout>;
  getWorkoutsByUserId: Array<Workout>;
  getPresetsByUserId: Array<Preset>;
  getUserById?: Maybe<User>;
  getAllUsers: Array<User>;
};


export type QueryGetWorkoutByIdArgs = {
  id: Scalars['ID'];
};


export type QueryGetWorkoutsByUserIdArgs = {
  userId: Scalars['ID'];
};


export type QueryGetPresetsByUserIdArgs = {
  userId: Scalars['ID'];
};


export type QueryGetUserByIdArgs = {
  id: Scalars['ID'];
};

export type Mutation = {
  __typename?: 'Mutation';
  login?: Maybe<Token>;
  registerWorkout?: Maybe<Workout>;
  signUp?: Maybe<Token>;
  createPreset?: Maybe<Preset>;
};


export type MutationLoginArgs = {
  username: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRegisterWorkoutArgs = {
  name: Scalars['String'];
  userId: Scalars['ID'];
  duration?: Maybe<Scalars['Float']>;
  calories?: Maybe<Scalars['Int']>;
  distance?: Maybe<Scalars['Float']>;
  presetId: Scalars['ID'];
};


export type MutationSignUpArgs = {
  email: Scalars['String'];
  username: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  password: Scalars['String'];
};


export type MutationCreatePresetArgs = {
  name: Scalars['String'];
  duration?: Maybe<Scalars['Boolean']>;
  calories?: Maybe<Scalars['Boolean']>;
  distance?: Maybe<Scalars['Boolean']>;
  userId: Scalars['ID'];
  type: Scalars['String'];
};

export type CreatePresetMutationVariables = Exact<{
  name: Scalars['String'];
  duration?: Maybe<Scalars['Boolean']>;
  calories?: Maybe<Scalars['Boolean']>;
  distance?: Maybe<Scalars['Boolean']>;
  userId: Scalars['ID'];
  type: Scalars['String'];
}>;


export type CreatePresetMutation = (
  { __typename?: 'Mutation' }
  & { createPreset?: Maybe<(
    { __typename?: 'Preset' }
    & Pick<Preset, 'id' | 'name' | 'duration' | 'calories' | 'distance' | 'userId' | 'type'>
  )> }
);

export type GetPresetsByUserIdQueryVariables = Exact<{
  userId: Scalars['ID'];
}>;


export type GetPresetsByUserIdQuery = (
  { __typename?: 'Query' }
  & { getPresetsByUserId: Array<(
    { __typename?: 'Preset' }
    & Pick<Preset, 'id' | 'userId' | 'name' | 'createdAt' | 'duration' | 'calories' | 'distance' | 'type'>
  )> }
);

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login?: Maybe<(
    { __typename?: 'Token' }
    & Pick<Token, 'token'>
  )> }
);

export type SignUpMutationVariables = Exact<{
  email: Scalars['String'];
  username: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  password: Scalars['String'];
}>;


export type SignUpMutation = (
  { __typename?: 'Mutation' }
  & { signUp?: Maybe<(
    { __typename?: 'Token' }
    & Pick<Token, 'token'>
  )> }
);


export const CreatePresetDocument = gql`
    mutation createPreset($name: String!, $duration: Boolean, $calories: Boolean, $distance: Boolean, $userId: ID!, $type: String!) {
  createPreset(name: $name, duration: $duration, calories: $calories, distance: $distance, userId: $userId, type: $type) {
    id
    name
    duration
    calories
    distance
    userId
    type
  }
}
    `;
export type CreatePresetMutationFn = ApolloReactCommon.MutationFunction<CreatePresetMutation, CreatePresetMutationVariables>;

/**
 * __useCreatePresetMutation__
 *
 * To run a mutation, you first call `useCreatePresetMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePresetMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPresetMutation, { data, loading, error }] = useCreatePresetMutation({
 *   variables: {
 *      name: // value for 'name'
 *      duration: // value for 'duration'
 *      calories: // value for 'calories'
 *      distance: // value for 'distance'
 *      userId: // value for 'userId'
 *      type: // value for 'type'
 *   },
 * });
 */
export function useCreatePresetMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreatePresetMutation, CreatePresetMutationVariables>) {
        return ApolloReactHooks.useMutation<CreatePresetMutation, CreatePresetMutationVariables>(CreatePresetDocument, baseOptions);
      }
export type CreatePresetMutationHookResult = ReturnType<typeof useCreatePresetMutation>;
export type CreatePresetMutationResult = ApolloReactCommon.MutationResult<CreatePresetMutation>;
export type CreatePresetMutationOptions = ApolloReactCommon.BaseMutationOptions<CreatePresetMutation, CreatePresetMutationVariables>;
export const GetPresetsByUserIdDocument = gql`
    query getPresetsByUserId($userId: ID!) {
  getPresetsByUserId(userId: $userId) {
    id
    userId
    name
    createdAt
    duration
    calories
    distance
    type
  }
}
    `;

/**
 * __useGetPresetsByUserIdQuery__
 *
 * To run a query within a React component, call `useGetPresetsByUserIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPresetsByUserIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPresetsByUserIdQuery({
 *   variables: {
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetPresetsByUserIdQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<GetPresetsByUserIdQuery, GetPresetsByUserIdQueryVariables>) {
        return ApolloReactHooks.useQuery<GetPresetsByUserIdQuery, GetPresetsByUserIdQueryVariables>(GetPresetsByUserIdDocument, baseOptions);
      }
export function useGetPresetsByUserIdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetPresetsByUserIdQuery, GetPresetsByUserIdQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<GetPresetsByUserIdQuery, GetPresetsByUserIdQueryVariables>(GetPresetsByUserIdDocument, baseOptions);
        }
export type GetPresetsByUserIdQueryHookResult = ReturnType<typeof useGetPresetsByUserIdQuery>;
export type GetPresetsByUserIdLazyQueryHookResult = ReturnType<typeof useGetPresetsByUserIdLazyQuery>;
export type GetPresetsByUserIdQueryResult = ApolloReactCommon.QueryResult<GetPresetsByUserIdQuery, GetPresetsByUserIdQueryVariables>;
export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  login(username: $username, password: $password) {
    token
  }
}
    `;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, baseOptions);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<LoginMutation>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const SignUpDocument = gql`
    mutation signUp($email: String!, $username: String!, $name: String, $password: String!) {
  signUp(username: $username, password: $password, name: $name, email: $email) {
    token
  }
}
    `;
export type SignUpMutationFn = ApolloReactCommon.MutationFunction<SignUpMutation, SignUpMutationVariables>;

/**
 * __useSignUpMutation__
 *
 * To run a mutation, you first call `useSignUpMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSignUpMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [signUpMutation, { data, loading, error }] = useSignUpMutation({
 *   variables: {
 *      email: // value for 'email'
 *      username: // value for 'username'
 *      name: // value for 'name'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useSignUpMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SignUpMutation, SignUpMutationVariables>) {
        return ApolloReactHooks.useMutation<SignUpMutation, SignUpMutationVariables>(SignUpDocument, baseOptions);
      }
export type SignUpMutationHookResult = ReturnType<typeof useSignUpMutation>;
export type SignUpMutationResult = ApolloReactCommon.MutationResult<SignUpMutation>;
export type SignUpMutationOptions = ApolloReactCommon.BaseMutationOptions<SignUpMutation, SignUpMutationVariables>;