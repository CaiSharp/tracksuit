import React from 'react';
import {Icon, TopNavigation, TopNavigationAction} from '@ui-kitten/components';
import {ImageProps} from 'react-native';
import {
	RenderProp,
	TouchableWithoutFeedback,
} from '@ui-kitten/components/devsupport';

const BackIcon: RenderProp<Partial<ImageProps>> = (props) => (
	<Icon {...props} name="arrow-back" />
);

type TopNavigationBack = {
	goBack: Function;
};

export const TopNavigationBack: React.FC<TopNavigationBack> = ({goBack}) => {
	const renderBackAction = () => (
		<TopNavigationAction icon={BackIcon} onPress={() => goBack()} />
	);
	return <TopNavigation alignment="center" accessoryLeft={renderBackAction} />;
};
