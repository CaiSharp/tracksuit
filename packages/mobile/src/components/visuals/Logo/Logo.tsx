import React from 'react';
import LogoSvg from '../../../assets/svg/logo.svg';
import {ViewStyle, View} from 'react-native';

type LogoProps = {
	style?: ViewStyle;
	height?: number;
	width?: number;
};

export const Logo: React.FC<LogoProps> = ({style, height, width}) => (
	<View style={style}>
		<LogoSvg height={height ?? 140} width={width ?? 140} />
	</View>
);
