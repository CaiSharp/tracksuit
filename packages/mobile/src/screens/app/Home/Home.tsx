import React, {useContext} from 'react';

import {TabNavigationProps} from '../../../routes/tabNavigastion';
import {Layout, Text, Button, Icon, useStyleSheet} from '@ui-kitten/components';
import {AuthContext} from '../../../context/authContext';
import {themedStyles} from './styles';

export const Home: React.FC<TabNavigationProps<'Home'>> = ({navigation}) => {
	const auth = useContext(AuthContext);

	console.log(auth.user);

	const styles = useStyleSheet(themedStyles);

	return (
		<Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
			<Text style={{textAlign: 'center', fontSize: 32}}>Home</Text>
			<Icon
				style={{width: 120, height: 120}}
				fill={styles.icon.backgroundColor}
				name="alert-triangle-outline"
			/>
			<Button onPress={() => auth.logout()}>Logout</Button>
		</Layout>
	);
};
