import {StyleService} from '@ui-kitten/components';

export const themedStyles = StyleService.create({
	icon: {
		backgroundColor: 'text-basic-color',
	},
});
