import React, {useContext} from 'react';

import {Layout, Text, Button} from '@ui-kitten/components';
import {AuthContext} from '../../../../context/authContext';
import {WorkoutNavigationProps} from '../../../../routes/stackNavigation';

export const Track: React.FC<WorkoutNavigationProps<'Track'>> = ({
	navigation,
}) => {
	const auth = useContext(AuthContext);

	console.log(auth.user);

	return (
		<Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
			<Text style={{textAlign: 'center', fontSize: 32}}>Track</Text>
		</Layout>
	);
};
