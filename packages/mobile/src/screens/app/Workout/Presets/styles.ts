import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
	layout: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	button: {
		width: 320,
		marginTop: 'auto',
		marginBottom: 20,
	},
	input: {
		width: 320,
		marginBottom: 10,
	},
	logo: {
		marginBottom: 40,
	},
	heading: {
		fontSize: 32,
		marginTop: 40,
		marginBottom: 40,
	},
});

export const {layout, button, heading, logo, input} = styles;
