import React, {useContext} from 'react';
import {useQuery} from '@apollo/client';
import {
	GetPresetsByUserIdDocument,
	GetPresetsByUserIdQuery,
} from '../../../../../graphql';
import {layout, button, heading} from './styles';

import {WorkoutNavigationProps} from '../../../../routes/stackNavigation';
import {
	Layout,
	Text,
	Button,
	List,
	ListItem,
	Icon,
} from '@ui-kitten/components';
import {AuthContext} from '../../../../context/authContext';
import {RenderProp} from '@ui-kitten/components/devsupport';
import {ImageProps} from 'react-native';
import {useFocusEventRefresh} from '../../../../hooks';

export const Presets: React.FC<WorkoutNavigationProps<'Presets'>> = ({
	navigation,
}) => {
	const auth = useContext(AuthContext);

	const {data, refetch} = useQuery<GetPresetsByUserIdQuery>(
		GetPresetsByUserIdDocument,
		{
			variables: {userId: auth.user?.id ?? ''},
		},
	);

	useFocusEventRefresh(navigation, refetch);

	const renderItemIcon: RenderProp<Partial<ImageProps>> = (props) => (
		<Icon {...props} name="radio-button-on-outline" />
	);

	const renderItem = ({
		item,
		index,
	}: {
		item: GetPresetsByUserIdQuery['getPresetsByUserId'][0];
		index: number;
	}) => (
		<ListItem
			style={{justifyContent: 'center', alignItems: 'center', width: 320}}
			title={`${item.name}`}
			description={`duration: ${item.duration} | calories: ${item.calories} | distance: ${item.distance} | type: ${item.type}`}
			accessoryLeft={renderItemIcon}
		/>
	);

	return (
		<Layout style={layout}>
			<Text style={heading}>Presets</Text>
			<List
				style={{
					flex: 1,
					width: '100%',
					backgroundColor: 'transparent',
				}}
				contentContainerStyle={{justifyContent: 'center', alignItems: 'center'}}
				data={data?.getPresetsByUserId}
				renderItem={renderItem}
			/>
			<Button
				style={button}
				size="large"
				onPress={() => navigation.navigate('Workouts', {screen: 'Create'})}>
				Create Preset
			</Button>
		</Layout>
	);
};
