import React, {useContext, useState} from 'react';

import {
	Layout,
	Input,
	Button,
	Toggle,
	Text,
	Select,
	SelectItem,
	IndexPath,
} from '@ui-kitten/components';
import {AuthContext} from '../../../../context/authContext';
import {WorkoutNavigationProps} from '../../../../routes/stackNavigation';
import {TopNavigationBack} from '../../../../components';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
	keyboard,
	layout,
	input,
	button,
	switchView,
	toggle,
	select,
} from './styles';
import {useMutation} from '@apollo/client';
import {
	CreatePresetDocument,
	CreatePresetMutation,
	WorkoutType,
} from '../../../../../graphql';
import {View} from 'react-native';

const workoutTypes = [
	WorkoutType['Cardio'],
	WorkoutType['FullBody'],
	WorkoutType['UpperBody'],
	WorkoutType['LowerBody'],
];

export const Create: React.FC<WorkoutNavigationProps<'Create'>> = ({
	navigation,
}) => {
	const auth = useContext(AuthContext);
	const [name, setName] = useState('');
	const [selection, setSelection] = React.useState<WorkoutType>(
		workoutTypes[0],
	);
	const [duration, setDuration] = useState(false);
	const [calories, setCalories] = useState(false);
	const [distance, setDistance] = useState(false);

	const [createPreset, {loading}] = useMutation<CreatePresetMutation>(
		CreatePresetDocument,
		{
			variables: {
				userId: auth.user?.id ?? '',
				name,
				duration,
				calories,
				distance,
				type: selection,
			},
			onCompleted: (data) => {
				navigation.goBack();
			},
		},
	);

	console.log(auth.user);
	console.log(typeof selection, selection);

	return (
		<>
			<TopNavigationBack goBack={navigation.goBack} />
			<KeyboardAwareScrollView contentContainerStyle={keyboard}>
				<Layout style={layout}>
					<Input
						placeholder="Name"
						label="Name"
						value={name}
						onChangeText={setName}
						style={input}
						autoCapitalize="none"
					/>
					<Select
						style={select}
						label="Workout Type"
						value={selection}
						onSelect={(index) => setSelection(workoutTypes[index - 1])}>
						<SelectItem title={WorkoutType.Cardio} />
						<SelectItem title={WorkoutType.FullBody} />
						<SelectItem title={WorkoutType.UpperBody} />
						<SelectItem title={WorkoutType.LowerBody} />
					</Select>
					<View style={switchView}>
						<Text style={toggle}>Distance</Text>
						<Toggle checked={distance} onChange={setDistance} />
					</View>
					<View style={switchView}>
						<Text style={toggle}>Calories</Text>
						<Toggle checked={calories} onChange={setCalories} />
					</View>
					<View style={switchView}>
						<Text style={toggle}>Time</Text>
						<Toggle checked={duration} onChange={setDuration} />
					</View>

					<Button
						size="large"
						style={button}
						onPress={() => {
							console.log(selection);
							createPreset();
						}}
						disabled={loading}>
						Add Preset
					</Button>
				</Layout>
			</KeyboardAwareScrollView>
		</>
	);
};
