import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
	keyboard: {flex: 1, backgroundColor: 'transparent'},
	layout: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	button: {
		width: 320,
		marginTop: 'auto',
		marginBottom: 20,
	},
	input: {
		width: 320,
		marginBottom: 20,
	},
	switchView: {
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		width: 320,
		marginBottom: 40,
	},
	toggle: {
		marginRight: 'auto',
	},
	select: {
		width: 320,
		marginBottom: 40,
	},
});

export const {
	layout,
	button,
	switchView,
	input,
	keyboard,
	toggle,
	select,
} = styles;
