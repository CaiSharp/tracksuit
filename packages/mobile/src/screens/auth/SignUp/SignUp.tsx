import React, {useState, useContext} from 'react';
import {AuthNavigationProps} from '../../../routes/stackNavigation';
import {Button, Layout, Input, Icon, Text} from '@ui-kitten/components';
import {useMutation} from '@apollo/client/react';
import {SignUpMutation, SignUpDocument} from '../../../../graphql';
import {AuthContext} from '../../../context/authContext';
import {TouchableWithoutFeedback, ImageProps} from 'react-native';
import {RenderProp} from '@ui-kitten/components/devsupport';
import {TopNavigationBack, Logo} from '../../../components';
import {layout, button, heading, logo, input, keyboard} from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export const SignUp: React.FC<AuthNavigationProps<'SignUp'>> = ({
	navigation,
}) => {
	const auth = useContext(AuthContext);
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [secureTextEntry, setSecureTextEntry] = useState(true);
	const [signUp, {loading}] = useMutation<SignUpMutation>(SignUpDocument, {
		variables: {username, password, name, email},
		onCompleted: (data) => {
			const token = data.signUp?.token;
			if (token) {
				auth.login(token);
			}
		},
	});

	const toggleSecureEntry = () => {
		setSecureTextEntry(!secureTextEntry);
	};

	const renderIcon: RenderProp<Partial<ImageProps>> = (props) => (
		<TouchableWithoutFeedback onPress={toggleSecureEntry}>
			<Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
		</TouchableWithoutFeedback>
	);

	return (
		<>
			<TopNavigationBack goBack={navigation.goBack} />

			<KeyboardAwareScrollView contentContainerStyle={keyboard}>
				<Layout style={layout}>
					<Text style={heading} category="h1">
						Tracksuit
					</Text>
					<Logo style={logo} />

					<Input
						placeholder="Name"
						label="Name"
						value={name}
						onChangeText={setName}
						style={input}
						disabled={loading}
					/>
					<Input
						placeholder="Email"
						label="Email"
						value={email}
						onChangeText={setEmail}
						style={input}
						disabled={loading}
						autoCapitalize="none"
					/>
					<Input
						placeholder="Username"
						label="Username"
						value={username}
						onChangeText={setUsername}
						style={input}
						disabled={loading}
					/>
					<Input
						placeholder="Password"
						value={password}
						accessoryRight={renderIcon}
						onChangeText={setPassword}
						label="Password"
						secureTextEntry={secureTextEntry}
						style={input}
						disabled={loading}
						autoCapitalize="none"
					/>
					<Button size="large" style={button} onPress={() => signUp()}>
						Sign Up
					</Button>
				</Layout>
			</KeyboardAwareScrollView>
		</>
	);
};
