import React, {useState, useContext} from 'react';
import {AuthNavigationProps} from '../../../routes/stackNavigation';
import {Button, Layout, Input, Icon, Text} from '@ui-kitten/components';
import {useMutation} from '@apollo/client/react';
import {LoginDocument, LoginMutation} from '../../../../graphql';
import {AuthContext} from '../../../context/authContext';
import {TopNavigationBack} from '../../../components';
import {layout, button, logo, heading, input, keyboard} from './styles';
import {RenderProp} from '@ui-kitten/components/devsupport';
import {ImageProps, TouchableWithoutFeedback} from 'react-native';
import {Logo} from '../../../components';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export const Login: React.FC<AuthNavigationProps<'Login'>> = ({navigation}) => {
	const auth = useContext(AuthContext);
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [secureTextEntry, setSecureTextEntry] = useState(true);
	const [login, {loading}] = useMutation<LoginMutation>(LoginDocument, {
		variables: {username, password},
		onCompleted: (data) => {
			const token = data.login?.token;
			if (token) {
				auth.login(token);
			}
		},
	});

	const toggleSecureEntry = () => {
		setSecureTextEntry(!secureTextEntry);
	};

	const renderIcon: RenderProp<Partial<ImageProps>> = (props) => (
		<TouchableWithoutFeedback onPress={toggleSecureEntry}>
			<Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
		</TouchableWithoutFeedback>
	);

	return (
		<>
			<TopNavigationBack goBack={navigation.goBack} />
			<KeyboardAwareScrollView contentContainerStyle={keyboard}>
				<Layout style={layout}>
					<Text style={heading} category="h1">
						Tracksuit
					</Text>
					<Logo style={logo} />

					<Input
						placeholder="Username"
						label="Username"
						value={username}
						onChangeText={setUsername}
						style={input}
						autoCapitalize="none"
					/>
					<Input
						placeholder="Password"
						value={password}
						accessoryRight={renderIcon}
						onChangeText={setPassword}
						label="Password"
						secureTextEntry={secureTextEntry}
						autoCapitalize="none"
						style={input}
						disabled={loading}
					/>
					<Button
						size="large"
						style={button}
						onPress={() => login()}
						disabled={loading}>
						Login
					</Button>
				</Layout>
			</KeyboardAwareScrollView>
		</>
	);
};
