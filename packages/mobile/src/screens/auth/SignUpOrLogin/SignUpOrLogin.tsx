import React from 'react';
import {AuthNavigationProps} from '../../../routes/stackNavigation';
import {Button, Layout, Text, TopNavigation} from '@ui-kitten/components';
import {layout, button, logo, heading, buttonContainer} from './styles';
import {Logo} from '../../../components';
import {View} from 'react-native';

export const SignUporLogin: React.FC<AuthNavigationProps<'SignUpOrLogin'>> = ({
	navigation,
}) => {
	return (
		<>
			<TopNavigation />
			<Layout style={layout}>
				<View style={heading}>
					<Text category="h1">Tracksuit</Text>
				</View>

				<Logo style={logo} />
				<View style={buttonContainer}>
					<Button
						style={button}
						size="large"
						onPress={() => navigation.navigate('Login')}>
						Login
					</Button>
					<Button
						appearance="outline"
						style={button}
						size="large"
						onPress={() => navigation.navigate('SignUp')}>
						Sign Up
					</Button>
				</View>
			</Layout>
		</>
	);
};
