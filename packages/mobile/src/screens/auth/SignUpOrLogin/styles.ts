import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
	layout: {
		flex: 1,
		justifyContent: 'flex-start',
		alignItems: 'center',
	},
	buttonContainer: {marginTop: 'auto'},
	button: {
		width: 320,
		marginBottom: 20,
	},
	logo: {
		marginBottom: 40,
	},
	heading: {
		marginTop: 40,
		marginBottom: 40,
	},
});

export const {layout, button, heading, logo, buttonContainer} = styles;
