import React from 'react';

import {Spinner, Layout} from '@ui-kitten/components';
import {layout} from './styles';

export const Loading: React.FC = ({}) => {
	return (
		<Layout style={layout}>
			<Spinner size="giant" />
		</Layout>
	);
};
