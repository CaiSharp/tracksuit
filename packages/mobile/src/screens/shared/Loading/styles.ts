import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
	layout: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});

export const {layout} = styles;
