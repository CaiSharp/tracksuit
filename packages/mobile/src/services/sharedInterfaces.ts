import { Role } from '../../graphql';

export type User = {
	id: string;
	username: string;
	email: string;
	roles: Role[];
};
