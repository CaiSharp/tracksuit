import { useReducer } from 'react';
import { User } from '../services/sharedInterfaces';

type authState = {
	isLoading: boolean;
	isLoggedIn: boolean;
	user: null | User;
};

type authAction =
	| { type: 'RESTORE_TOKEN'; token: string }
	| { type: 'LOGIN'; token: string }
	| { type: 'LOGOUT' };

const reducer = (prevState: authState, action: authAction) => {
	switch (action.type) {
		case 'RESTORE_TOKEN':
			return {
				...prevState,
				// user: action.token,
				isLoading: false,
			};
		case 'LOGIN':
			return {
				...prevState,
				isLoggedIn: true,
				// user: action.token,
			};
		case 'LOGOUT':
			return {
				...prevState,
				isLoggedIn: false,
				user: null,
			};
	}
};

const authInitState: authState = {
	isLoading: false,
	isLoggedIn: false,
	user: null,
};

export const authReducer = () => useReducer(reducer, authInitState);
