import {
	createBottomTabNavigator,
	BottomTabNavigationProp,
} from '@react-navigation/bottom-tabs';
import {RouteProp} from '@react-navigation/native';
import {WorkoutStackParamList} from './stackNavigation';

export type TabParamList = {
	Home: undefined;
	Workouts: {
		screen?: keyof WorkoutStackParamList;
		//no type guard here, pay attention that you only use the required params
		params?: WorkoutStackParamList[keyof WorkoutStackParamList];
	};
};

type TabNavigationProp<
	RouteName extends keyof TabParamList
> = BottomTabNavigationProp<TabParamList, RouteName>;

type TabRouteProp<RouteName extends keyof TabParamList> = RouteProp<
	TabParamList,
	RouteName
>;

// import this in your screens as navigation prop
export type TabNavigationProps<RouteName extends keyof TabParamList> = {
	navigation: TabNavigationProp<RouteName>;
	route: TabRouteProp<RouteName>;
};

export const Tab = createBottomTabNavigator();
