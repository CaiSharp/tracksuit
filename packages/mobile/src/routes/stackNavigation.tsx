import {RouteProp, CompositeNavigationProp} from '@react-navigation/native';
import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';
import {TabParamList} from './tabNavigastion';
import {
	StackNavigationProp,
	createStackNavigator,
} from '@react-navigation/stack';

type AuthStackParamList = {
	Login: undefined;
	SignUp: undefined;
	SignUpOrLogin: undefined;
};

type AuthNavigationProp<
	RouteName extends keyof AuthStackParamList
> = StackNavigationProp<AuthStackParamList, RouteName>;

type AuthRouteProp<RouteName extends keyof AuthStackParamList> = RouteProp<
	AuthStackParamList,
	RouteName
>;

// import this in your screens as navigation prop
export type AuthNavigationProps<RouteName extends keyof AuthStackParamList> = {
	navigation: AuthNavigationProp<RouteName>;
	route: AuthRouteProp<RouteName>;
};

export const AuthStack = createStackNavigator<AuthStackParamList>();

export type WorkoutStackParamList = {
	Presets: undefined;
	Create: undefined;
	Track: undefined;
};

type WorkoutNavigationProp = CompositeNavigationProp<
	BottomTabNavigationProp<TabParamList, 'Workouts'>,
	StackNavigationProp<WorkoutStackParamList>
>;

type WorkoutRouteProp<
	RouteName extends keyof WorkoutStackParamList
> = RouteProp<WorkoutStackParamList, RouteName>;

// import this in your screens as navigation prop
export type WorkoutNavigationProps<
	RouteName extends keyof WorkoutStackParamList
> = {
	navigation: WorkoutNavigationProp;
	route: WorkoutRouteProp<RouteName>;
};

export const WorkoutStack = createStackNavigator<WorkoutStackParamList>();
