import React from 'react';
import jwtDecode from 'jwt-decode';
import { User } from '../services/sharedInterfaces';
import AsyncStorage from '@react-native-community/async-storage';

export const initUser = async (): Promise<User | null> => {
	const token = await AsyncStorage.getItem('token');
	if (token) {
		const { user }: { user: User } = jwtDecode(token);
		return user;
	} else {
		return null;
	}
};

export const login = async (
	stateSetter: React.Dispatch<React.SetStateAction<User | null | undefined>>,
	token: string,
) => {
	AsyncStorage.setItem('token', token);
	const { user }: { user: User } = jwtDecode(token);
	stateSetter(user);
};

export const logout = async (
	stateSetter: React.Dispatch<React.SetStateAction<User | null | undefined>>,
) => {
	AsyncStorage.removeItem('token');
	stateSetter(null);
};

export const AuthContext = React.createContext({
	user: { id: '', username: '', email: '', roles: [] } as
		| User
		| null
		| undefined,
	login: (token: string): void => {},
	logout: (): void => {},
});
