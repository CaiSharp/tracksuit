import {useEffect} from 'react';
import {NavigationProp} from '@react-navigation/native';

export const useFocusEventRefresh = (
	navigation: NavigationProp<any>,
	refetch: () => Promise<any>,
) =>
	useEffect(() => {
		const focusListener = navigation.addListener('focus', () => {
			refetch();
		});
		return () => {
			navigation.removeListener('focus', focusListener);
		};
	}, []);
