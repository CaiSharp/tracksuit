import {registerRootComponent} from 'expo';
import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {AppearanceProvider} from 'react-native-appearance';
import {useColorScheme} from 'react-native-appearance';

import {
	ApolloClient,
	InMemoryCache,
	ApolloProvider,
	HttpLink,
	ApolloLink,
} from '@apollo/client';

import {App} from './App';

const httpLink = new HttpLink({
	uri: 'http://localhost:4000/graphql',
});

const authLink = new ApolloLink((operation, forward) => {
	AsyncStorage.getItem('token').then((token) => {
		operation.setContext({
			headers: {
				authorization: token ? `Bearer ${token}` : '',
			},
		});
	});
	return forward(operation);
});

const client = new ApolloClient({
	cache: new InMemoryCache(),
	link: authLink.concat(httpLink),
});

const Main: React.FC = () => {
	const colorScheme = useColorScheme();
	return (
		<ApolloProvider client={client}>
			<AppearanceProvider>
				<IconRegistry icons={EvaIconsPack} />
				<ApplicationProvider
					{...eva}
					theme={colorScheme === 'dark' ? eva.dark : eva.light}>
					<App />
				</ApplicationProvider>
			</AppearanceProvider>
		</ApolloProvider>
	);
};

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in the Expo client or in a native build,
// the environment is set up appropriately
registerRootComponent(Main);
