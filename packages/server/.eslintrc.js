module.exports = {
	env: {
		browser: true,
		es6: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'plugin:@typescript-eslint/recommended',
		'prettier',
		'prettier/@typescript-eslint',
	],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly',
	},
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2020,
		sourceType: 'module',
	},
	plugins: ['@typescript-eslint'],
	rules: {
		'@typescript-eslint/explicit-function-return-type': 'off',
		'@typescript-eslint/no-this-alias': 'off',
		//'@typescript-eslint/no-explicit-any': 'off',
		//'@typescript-eslint/no-object-literal-type-assertion': 'off',
		//'@typescript-eslint/no-unused-vars': 'off',
		//curly: 'warn',
		'no-unneeded-ternary': ['warn', { defaultAssignment: false }],
		'object-shorthand': 'warn',
	},
	ignorePatterns: ['**/*.test.ts'],
};
