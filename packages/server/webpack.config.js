const path = require('path');
const nodeExternals = require('webpack-node-externals');
const { CheckerPlugin } = require('awesome-typescript-loader');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = {
	entry: {
		server: './src/app.ts',
	},
	output: {
		path: path.join(__dirname, 'build'),
		publicPath: '/',
		filename: 'app.js',
	},
	mode: 'production',
	target: 'node',
	externals: [
		nodeExternals(),
		nodeExternals({
			modulesDir: path.resolve(__dirname, '../../node_modules'),
		}),
	],
	module: {
		rules: [
			{
				test: /\.mjs$/,
				include: /node_modules/,
				type: 'javascript/auto',
			},
			{
				test: /\.tsx?$/,
				use: 'awesome-typescript-loader',
				exclude: /node_modules/,
			},
		],
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
		alias: {
			'@fitapp/api/server': path.resolve(__dirname, '../api/server'),
		},
		plugins: [new TsconfigPathsPlugin()],
	},
	plugins: [new CheckerPlugin(), new CleanWebpackPlugin()],
};
