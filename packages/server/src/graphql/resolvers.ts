import {
	login,
	registerWorkout,
	createPreset,
	signup,
} from '@controller/index';
import { Resolvers, Role } from '@fitapp/api/server';
import User from '@models/user';
import Workout from '@models/workout';
import Preset from '@models/preset';
import { isObjectId, isUserId } from '@services/sharedUtils';

export const resolvers: Resolvers = {
	Query: {
		getAllUsers: async (parent, args) => {
			return User.find({});
		},
		getUserById: async (parent, args) => {
			const { id } = args;
			isObjectId(id);
			return User.findOne({ _id: id })
				.populate('workouts')
				.populate('exercises');
		},
		getWorkoutsByUserId: async (parent, args, context, info) => {
			const { userId } = args;
			isObjectId(userId);
			return Workout.find({ userId });
		},
		getWorkoutById: async (parent, args) => {
			const { id } = args;
			isObjectId(id);
			return Workout.findOne({ _id: id });
		},
		getPresetsByUserId: async (parent, args, context, info) => {
			const { userId } = args;
			isObjectId(userId);
			return Preset.find({ userId });
		},
	},
	Mutation: {
		login: async (parent, args) => {
			const { username, password } = args;
			const token = await login(username, password);
			return { token };
		},
		registerWorkout: async (parent, args) => {
			//TODO: Transaction
			const { userId } = args;
			const user = await isUserId(userId);
			const workout = await registerWorkout(user, {
				duration: null,
				calories: null,
				name: null,
				distance: null,
				...args,
			});
			return workout;
		},
		signUp: async (parent, args) => {
			const token = await signup(args, [Role.User]);
			return { token };
		},
		createPreset: async (parent, args) => {
			const { userId } = args;
			const user = await isUserId(userId);
			const preset = await createPreset(user, {
				duration: null,
				calories: null,
				name: null,
				distance: null,
				...args,
			});
			return preset;
		},
	},
};
