import {
	AuthenticationError,
	SchemaDirectiveVisitor,
} from 'apollo-server-express';
import { defaultFieldResolver } from 'graphql';

import { UserDocument } from '@models/user';

export class AuthDirective extends SchemaDirectiveVisitor {
	public visitFieldDefinition(field) {
		const { resolve = defaultFieldResolver } = field;
		field.resolve = async function(...args) {
			const [, , context] = args;
			const { user }: { user: UserDocument } = context;

			if (!user) {
				throw new AuthenticationError('Unauthorized');
			}

			return resolve.apply(this, args);
		};
	}
}

export class RoleDirective extends SchemaDirectiveVisitor {
	public visitFieldDefinition(field) {
		const { resolve = defaultFieldResolver } = field;
		const roles: string[] = this.args.roles;

		field.resolve = async function(...args) {
			const [, , context] = args;
			const { user }: { user: UserDocument } = context;

			if (!user || !user.roles) {
				throw new AuthenticationError('Unauthorized');
			}

			if (!user.roles.some(el => roles.includes(el))) {
				//return null instead of auth error so other fields still get returned
				return null;
				//throw new AuthenticationError('No Permission');
			}

			return resolve.apply(this, args);
		};
	}
}
