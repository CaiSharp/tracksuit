import { WorkoutDocument } from '@models/workout';
import { MutationRegisterWorkoutArgs, RequireFields } from '@fitapp/api/server';
import { UserDocument } from '@models/user';
import Workout from '@models/workout';

export const registerWorkout = async (
	user: UserDocument,
	args: RequireFields<
		MutationRegisterWorkoutArgs,
		'calories' | 'distance' | 'duration' | 'name' | 'presetId' | 'userId'
	>,
): Promise<WorkoutDocument> => {
	//create workout with empty exercises first, update later
	const workout = await Workout.create({ ...args, createdAt: null });

	await user.updateOne({ $push: { workouts: workout.id } }).catch(() => {
		throw new Error(`Couldn't update the user workouts`);
	});

	return workout;
};
