import User from '../models/user';
import { AuthenticationError } from 'apollo-server-express';

import { createJWT } from '@services/sharedUtils';
import { RequireFields, MutationSignUpArgs, Role } from '@fitapp/api/server';

export const signup = async (
	args: RequireFields<MutationSignUpArgs, 'username' | 'password' | 'email'>,
	roles: Role[],
): Promise<string> => {
	const user = await User.create({ ...args, roles, createdAt: null });
	if (!user) {
		throw new AuthenticationError('No user was created');
	}
	return createJWT(user);
};
