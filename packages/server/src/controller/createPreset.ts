import { PresetDocument } from '@models/preset';
import { MutationCreatePresetArgs, RequireFields } from '@fitapp/api/server';
import { UserDocument } from '@models/user';
import Preset from '@models/preset';

export const createPreset = async (
	user: UserDocument,
	args: RequireFields<
		MutationCreatePresetArgs,
		'calories' | 'distance' | 'duration' | 'name' | 'type' | 'userId'
	>,
): Promise<PresetDocument> => {
	const preset = await Preset.create({ ...args, createdAt: null });

	await user.updateOne({ $push: { presets: preset.id } }).catch(() => {
		throw new Error(`Couldn't update the user presets`);
	});

	return preset;
};
