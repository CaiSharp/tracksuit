import { UserInputError } from 'apollo-server-core';
import jwt from 'jsonwebtoken';
import { ObjectId } from 'mongodb';

import { User } from '@fitapp/api/server';
import UserModel from '@models/user';

const { JWT_SECRET } = process.env;

//check if valid ID before hitting the db
export const isObjectId = (id: string): void => {
	if (!ObjectId.isValid(id)) {
		throw new UserInputError(`${id} is not a valid ID`);
	}
};
export const isUserId = async (userId: string) => {
	isObjectId(userId);
	const user = await UserModel.findById(userId);
	if (!user) {
		throw new UserInputError(`${userId} is not a valid user ID`);
	}
	return user;
};

export const createJWT = (user: User) => {
	const token = jwt.sign(
		{
			user: {
				id: user.id,
				username: user.username,
				email: user.email,
				roles: user.roles,
			},
		},
		JWT_SECRET,
		{ expiresIn: '100y' },
	);
	return token;
};
