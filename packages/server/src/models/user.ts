import bcryptjs from 'bcryptjs';
import mongoose, { Model, Schema } from 'mongoose';

import { Role, User } from '@fitapp/api/server';

const userSchema = new Schema(
	{
		name: {
			type: String,
			minlength: 3,
		},
		username: {
			type: String,
			unique: true,
			required: true,
			minlength: 3,
		},
		password: {
			type: String,
			required: true,
		},
		email: {
			type: String,
			unique: true,
			required: true,
		},
		roles: { type: [String], enum: [...Object.values(Role)] },
		workouts: [{ type: Schema.Types.ObjectId, ref: 'Workout' }],
		presets: [{ type: Schema.Types.ObjectId, ref: 'Preset' }],
	},
	{ timestamps: true },
);

export type UserDocument = mongoose.Document &
	User & {
		_id: mongoose.Document['_id'];
		isValidPassword: (password: string) => Promise<boolean>;
	};

export type UserModel = Model<UserDocument>;

//hashes whenever pw is modified
userSchema.pre<UserDocument>('save', async function (this, next) {
	if (this.isModified('password')) {
		const salt = await bcryptjs.genSalt(10);
		this.password = await bcryptjs.hash(this.password, salt);
		next();
	}
	next();
});

userSchema.methods.isValidPassword = async function (password: string) {
	const user: UserDocument = this;
	const compareResult = await bcryptjs.compare(password, user.password);
	return compareResult;
};

export default mongoose.model<UserDocument, UserModel>('User', userSchema);
