import mongoose, { Model, Schema } from 'mongoose';

import { Preset, WorkoutType } from '@fitapp/api/server';

export const presetSchema = new Schema(
	{
		userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		name: {
			type: String,
			required: true,
			minlength: 3,
		},
		duration: { type: Boolean },
		calories: { type: Boolean },
		distance: { type: Boolean },
		type: { type: String, required: true },
	},
	{ timestamps: true },
);

export type PresetDocument = Preset &
	mongoose.Document & {
		_id: mongoose.Document['_id'];
	};

export type PresetModel = Model<PresetDocument>;

export default mongoose.model<PresetDocument, PresetModel>(
	'Preset',
	presetSchema,
);
