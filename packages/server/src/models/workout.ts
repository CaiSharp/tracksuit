import mongoose, { Model, Schema } from 'mongoose';

import { Workout } from '@fitapp/api/server';

export const workoutSchema = new Schema(
	{
		userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
		presetId: { type: Schema.Types.ObjectId, ref: 'Preset', required: true },
		name: {
			type: String,
			required: true,
			minlength: 3,
		},
		duration: { type: Number },
		calories: { type: Number },
		distance: { type: Number },
	},
	{ timestamps: true },
);

export type WorkoutDocument = Workout &
	mongoose.Document & {
		_id: mongoose.Document['_id'];
	};

export type WorkoutModel = Model<WorkoutDocument>;

export default mongoose.model<WorkoutDocument, WorkoutModel>(
	'Workout',
	workoutSchema,
);
